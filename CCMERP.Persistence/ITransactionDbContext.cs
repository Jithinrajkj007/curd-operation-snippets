﻿using CCMERP.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Threading.Tasks;

namespace CCMERP.Persistence
{
    public interface IDbContext : IDisposable, IInfrastructure<IServiceProvider>
    {



    }
    public interface ITransactionDbContext: IDbContext
    {
        DbSet<OrganizationCustomerMapping> organizationCustomerMappings { get; set; }
        DbSet<Customer> Customers { get; set; }
        DbSet<UOMMaster> uOMMaster { get; set; }
        DbSet<ItemMaster> itemmaster { get; set; }
        DbSet<KeyMaster> keymaster { get; set; }
        DbSet<SalesOrderDtl> salesorderdtl { get; set; }
        DbSet<SalesOrderHeader> salesorderheader { get; set; }
        DatabaseFacade Database { get; }

        Task<int> SaveChangesAsync();
    }
}
