﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.PrivilegeService.Commands
{
    public class CreatePrivilegeCommand : IRequest<Response<int>>
    {
        [Required]
        public int PrivilegeID { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string PrivilegeName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string PrivilegeCode { get; set; }
        [Required]
        public double PrivilegeAmount { get; set; }
        [Required]
        public decimal PrivilegePrice { get; set; }
        //[Required]
        //public DateTime PrivilegeDate { get; set; }


        public class CreatePrivilegeCommandHandler : IRequestHandler<CreatePrivilegeCommand, Response<int>>
        {
            private readonly IdentityContext _context;

            //private readonly ITransactionDbContext _tcontext;
            public CreatePrivilegeCommandHandler(IdentityContext context)//, ITransactionDbContext tcontext)
            {
                _context = context;
                //_tcontext = tcontext;
            }
            public async Task<Response<int>> Handle(CreatePrivilegeCommand request, CancellationToken cancellationToken)
            {

                try
                {


                    var pre = _context.privilege.Where(a => a.PrivilegeName.ToLower() == request.PrivilegeName.ToLower()).ToList();
                    if (pre.Count <= 0)
                    {

                        PrivilegeMaster privilege = new()
                        {
                           
                            PrivilegeID = request.PrivilegeID,
                            PrivilegeName = request.PrivilegeName,
                            PrivilegeCode = request.PrivilegeCode,
                            PrivilegeAmount = request.PrivilegeAmount,
                            PrivilegePrice = request.PrivilegePrice,
                            //PrivilegeDate = request.PrivilegeDate,
                            PrivilegeDate = DateTime.Now,
                        };

                        _context.privilege.Add(privilege);
                        await _context.SaveChangesAsync();

                        return new Response<int>(privilege.PrivilegeID, "Success", true);
                    }
                    else
                    {
                        return new Response<int>(0, "A Privilege with the same name already exists", false);
                    }


                }
                catch (Exception ex)
                {

                    var currentExpection = ex;
                    return new Response<int>(0, "Exception", false);
                }


            }
        }



    }
}