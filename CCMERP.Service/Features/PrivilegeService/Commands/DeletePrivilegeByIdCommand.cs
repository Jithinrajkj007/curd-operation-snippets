﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.Privilege.Commands
{
   public class DeletePrivilegeByIdCommand : IRequest<Response<int>>
    {

        [Required]      
        public int privilegeId { get; set; }

        public class DeletePrivilegeByIdCommandHandler : IRequestHandler<DeletePrivilegeByIdCommand, Response<int>>
        {
            
            private readonly IdentityContext _context;
            public DeletePrivilegeByIdCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(DeletePrivilegeByIdCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var privilege = await _context.privilege.FindAsync(request.privilegeId);
                    if (privilege == null)
                    {
                        return new Response<int>(0, "No privilege found ", false);
                    }
                    else
                    {
                        var deletePrivilegeId = request.privilegeId;
                        //privilege.IsActive = 0;
                        _context.Remove(privilege);
                        await _context.SaveChangesAsync();                      

                        return new Response<int>(deletePrivilegeId, "privilege deleted successfully", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }


            }
        }
    }
}