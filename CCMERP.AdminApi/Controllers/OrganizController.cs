﻿using CCMERP.Service.Features.OrganizationsService.Commands;
using CCMERP.Service.Features.OrganizationsService.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace CCMERP.AdminApi.Controllers
{
    [Route("api/V1/Organiz")]
    [ApiController]
    public class OrganizController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        [HttpPost]
        public async Task<IActionResult> CreateOne(CreateOrganizationsCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet]       
        public async Task<IActionResult> GetAllone(int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetAllOrganizationsQuery { PageNumber = pageNumber, _pageSize = pageSize }));
        }

        [HttpGet("{orgID}")]
        public async Task<IActionResult> GetByIdOne(int orgID)
        {
            return Ok(await Mediator.Send(new GetorganizationByIdQuery { orgID = orgID }));
        }

        [HttpDelete("{orgID}")]
        public async Task<IActionResult> DeleteOne(int orgID)
        {
            return Ok(await Mediator.Send(new DeleteOrganizationsByIdCommand { Org_ID = orgID }));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateOne(UpdateOrganizationsCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
