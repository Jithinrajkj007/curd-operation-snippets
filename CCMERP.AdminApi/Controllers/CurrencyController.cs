﻿//using CCMERP.Service.Features.CountryService.Commands;
using CCMERP.Service.Features.CurrencyService.Commands;
using CCMERP.Service.Features.CurrencyService.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace CCMERP.AdminApi.Controllers
{
    [Route("api/V1/Currency")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();


        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> GetAll(int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetAllCurrencyQuery { PageNumber = pageNumber, _pageSize = pageSize }));
        }


        [HttpGet("{currencyId}")]
        public async Task<IActionResult> GetById(int currencyId)
        {
            return Ok(await Mediator.Send(new GetCurrencyByIdQuery { currencyId = currencyId }));
        }

       
        [HttpDelete("{currencyId}")]
        public async Task<IActionResult> Delete(int currencyId)
        {
            return Ok(await Mediator.Send(new DeleteCurrencyByIdCommand { currencyId = currencyId }));
        }

       
       [HttpPost]
       public async Task<IActionResult> Create(CreateCurrencyCommand command)
       {
           return Ok(await Mediator.Send(command));
       }

     

      [HttpPut]
      public async Task<IActionResult> Update(UpdateCurrencyCommand command)
      {
          return Ok(await Mediator.Send(command));
      }
    }
}
