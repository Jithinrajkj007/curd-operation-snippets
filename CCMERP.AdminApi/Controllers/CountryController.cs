﻿using CCMERP.Service.Features.CountryService.Commands;
using CCMERP.Service.Features.CountryService.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace CCMERP.AdminApi.Controllers
{
    [Route("api/V1/Country")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();


        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> GetAll(int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetAllCountryQuery { PageNumber = pageNumber, _pageSize = pageSize }));
        }


        [HttpGet("{countryId}")]
        public async Task<IActionResult> GetById(int countryId)
        {
            return Ok(await Mediator.Send(new GetCountryByIdQuery { countryId = countryId }));
        }


        [HttpDelete("{countryId}")]
        public async Task<IActionResult> Delete(int countryId)
        {
            return Ok(await Mediator.Send(new DeleteCountryByIdCommand { countryId = countryId }));
        }


        [HttpPost]
        public async Task<IActionResult> Create(CreateCountryCommand command)
        {
            return Ok(await Mediator.Send(command));
        }



        [HttpPut]
        public async Task<IActionResult> Update(UpdateCountryCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
