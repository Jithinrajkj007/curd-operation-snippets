﻿//using CCMERP.Service.Features.CountryService.Commands;
using CCMERP.Service.Features.Privilege.Commands;
using CCMERP.Service.Features.PrivilegeService.Commands;
using CCMERP.Service.Features.PrivilegeService.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;


namespace CCMERP.AdminApi.Controllers
{
    [Route("api/V1/Privilege")]
    [ApiController]
    public class PrivilegeController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> GetAll(int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetAllPrivilegeQuery { PageNumber = pageNumber, _pageSize = pageSize }));
        }

        [HttpGet("{privilegeId}")]
        public async Task<IActionResult> GetById(int privilegeId)
        {
            return Ok(await Mediator.Send(new GetPrivilegeByIdQuery { privilegeId = privilegeId }));
        }

        [HttpDelete("{privilegeId}")]
        public async Task<IActionResult> Delete(int privilegeId)
        {
            return Ok(await Mediator.Send(new DeletePrivilegeByIdCommand { privilegeId = privilegeId }));
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreatePrivilegeCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut]
        public async Task<IActionResult> Update(UpdatePrivilegeCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
