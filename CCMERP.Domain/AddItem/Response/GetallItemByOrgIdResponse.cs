﻿using CCMERP.Domain.AddItem.Request;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.AddItem.Response
{
    public class GetallItemByOrgIdResponse:PaginationResponse
    {
        public List<AddItemsRequest> items { get; set; }
    }

    public class GetallItemByIdResponse
    {
        public AddItemsRequest item { get; set; }
    }

    public class GetallItemByIdAutoCompleteResponse
    {
        public List<AddItemsRequest> items { get; set; }
    }
}
