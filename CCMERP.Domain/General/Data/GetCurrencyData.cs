﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.General.Data
{
    public class GetCurrencyData
    {
        public int currencyId { get; set; }
        public string currencyName { get; set; }
        public string currencyABR { get; set; }

    }
}
