﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Order.Data
{
    public class ImportOrder
    {
        public int LINE_ID { get; set; }
        public DateTime DUE_DATE { get; set; }
        public string STOCK_CODE { get; set; }
        public int QTY_REQUIRED { get; set; }
    }
}
