﻿using CCMERP.Domain.Entities;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Organizations.Response
{
    public class GetAllOrganizationsResponse : PaginationResponse
    {
        public List<Organization> organizations { get; set; }
       
    }
}
