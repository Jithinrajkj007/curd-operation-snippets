﻿using CCMERP.Domain.Entities;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Privilege.Response
{
    public class GetAllPrivilegeResponse : PaginationResponse
    {
        public List<Entities.PrivilegeMaster> Privileges { get; set; }
    }
}
